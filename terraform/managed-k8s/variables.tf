variable "cloud_id" {
  description = "The ID of Cloud to apply any resources to."
  type        = string
  sensitive   = true
}

variable "folder_id" {
  description = "The Folder ID to operate under, if not specified by a given resource."
  type        = string
  sensitive   = true
}

variable "zone" {
  description = "The availability zone where the machine will be created."
  default     = "ru-central1-a"
  type        = string

  validation {
    condition     = contains(toset(["ru-central1-a", "ru-central1-b", "ru-central1-c"]), var.zone)
    error_message = "Select availability zone from the list: ru-central1-a, ru-central1-b, ru-central1-c."
  }
}

variable "k8s_version" {
  description = "Version of Kubernetes that will be used for master."
  default     = "1.22"
  type        = string
}

variable "nodes_admin_network_cidr_blocks" {
  description = "Networks from which administrators will connect to the nodes over SSH."
  type        = list(string)
}

variable "nodes_admin_ssh_public_key" {
  description = "SSH key for connecting to cluster nodes."
  type        = string
}

variable "api_admin_network_cidr_blocks" {
  description = "Networks from which administrators will connect to the kubernetes API."
  default     = ["0.0.0.0/0"] # Адреса не ограничены, т.к. неизвестен IP, с которого GitLab будет деплоить приложение
  type        = list(string)
}
