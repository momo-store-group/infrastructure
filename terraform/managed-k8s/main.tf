# SA

resource "yandex_iam_service_account" "k8s_admin" {
  name = "k8s-admin"
}

resource "yandex_resourcemanager_folder_iam_member" "k8s_admin" {
  folder_id = var.folder_id
  role      = "k8s.admin"
  member    = "serviceAccount:${yandex_iam_service_account.k8s_admin.id}"
}

resource "yandex_resourcemanager_folder_iam_member" "clusters_agent" {
  folder_id = var.folder_id
  role      = "k8s.clusters.agent"
  member    = "serviceAccount:${yandex_iam_service_account.k8s_admin.id}"
}

resource "yandex_resourcemanager_folder_iam_member" "vpc_public_admin" {
  folder_id = var.folder_id
  role      = "vpc.publicAdmin"
  member    = "serviceAccount:${yandex_iam_service_account.k8s_admin.id}"
}

resource "yandex_resourcemanager_folder_iam_member" "load_balancer_admin" {
  folder_id = var.folder_id
  role      = "load-balancer.admin"
  member    = "serviceAccount:${yandex_iam_service_account.k8s_admin.id}"
}

resource "yandex_iam_service_account" "k8s_images_puller" {
  name = "k8s-images-puller"
}

resource "yandex_resourcemanager_folder_iam_member" "images_puller" {
  folder_id = var.folder_id
  role      = "container-registry.images.puller"
  member    = "serviceAccount:${yandex_iam_service_account.k8s_images_puller.id}"
}

# ----------------------------------------------------------------------------------------------------------------------
# VPC

resource "yandex_vpc_network" "this" {
  name = "k8s-network"
}

resource "yandex_vpc_subnet" "this" {
  network_id     = yandex_vpc_network.this.id
  v4_cidr_blocks = ["10.1.0.0/16"]
  zone           = var.zone
}

resource "yandex_vpc_security_group" "main" {
  network_id  = yandex_vpc_network.this.id
  name        = "k8s-main"
  description = "Group rules ensure the basic performance of the cluster. Apply it to the cluster and node groups."

  ingress {
    protocol       = "TCP"
    description    = "Rule allows incoming traffic from the internet to the NodePort port range."
    v4_cidr_blocks = ["0.0.0.0/0"]
    from_port      = 30000
    to_port        = 32767
  }

  ingress {
    protocol          = "ANY"
    description       = "Rule allows master-node and node-node communication inside a security group."
    predefined_target = "self_security_group"
    from_port         = 0
    to_port           = 65535
  }

  ingress {
    protocol          = "TCP"
    description       = "Rule allows availability checks from load balancer's address range."
    predefined_target = "loadbalancer_healthchecks"
    from_port         = 0
    to_port           = 65535
  }

  ingress {
    protocol       = "ANY"
    description    = "Rule allows pod-pod and service-service communication."
    v4_cidr_blocks = ["10.0.0.0/8"]
    from_port      = 0
    to_port        = 65535
  }

  ingress {
    protocol       = "ICMP"
    description    = "Rule allows debugging ICMP packets from internal subnets."
    v4_cidr_blocks = ["10.0.0.0/8", "172.16.0.0/12", "192.168.0.0/16"]
  }

  egress {
    protocol       = "ANY"
    description    = "Rule allows all outgoing traffic."
    v4_cidr_blocks = ["0.0.0.0/0"]
    from_port      = 0
    to_port        = 65535
  }
}

resource "yandex_vpc_security_group" "nodes_ssh_access" {
  network_id  = yandex_vpc_network.this.id
  name        = "k8s-nodes-ssh-access"
  description = "Group rules allow connections to cluster nodes over SSH. Apply the rules only for node groups."

  ingress {
    protocol       = "TCP"
    description    = "Rule allows connections to nodes over SSH from specified IPs."
    v4_cidr_blocks = var.nodes_admin_network_cidr_blocks
    port           = 22
  }
}

resource "yandex_vpc_security_group" "master_whitelist" {
  network_id  = yandex_vpc_network.this.id
  name        = "k8s-master-whitelist"
  description = "Group rules allow access to the Kubernetes API from the internet. Apply the rules to the cluster only."

  ingress {
    protocol       = "TCP"
    description    = "Rule allows connections to the Kubernetes API via port 6443 from a specified network."
    v4_cidr_blocks = var.api_admin_network_cidr_blocks
    port           = 6443
  }

  ingress {
    protocol       = "TCP"
    description    = "Rule allows connections to the Kubernetes API via port 443 from a specified network."
    v4_cidr_blocks = var.api_admin_network_cidr_blocks
    port           = 443
  }
}

# ----------------------------------------------------------------------------------------------------------------------
# KMS

resource "yandex_kms_symmetric_key" "this" {
  name              = "k8s-kms-key"
  default_algorithm = "AES_128"
  rotation_period   = "8760h"
}

resource "yandex_kms_symmetric_key_iam_binding" "viewer" {
  symmetric_key_id = yandex_kms_symmetric_key.this.id
  role             = "viewer"
  members          = ["serviceAccount:${yandex_iam_service_account.k8s_admin.id}"]
}

# ----------------------------------------------------------------------------------------------------------------------
# Cluster

resource "yandex_kubernetes_cluster" "this" {
  network_id              = yandex_vpc_network.this.id
  service_account_id      = yandex_iam_service_account.k8s_admin.id
  node_service_account_id = yandex_iam_service_account.k8s_images_puller.id

  master {
    version = var.k8s_version

    zonal {
      zone      = var.zone
      subnet_id = yandex_vpc_subnet.this.id
    }

    public_ip = true

    security_group_ids = [
      yandex_vpc_security_group.main.id,
      yandex_vpc_security_group.master_whitelist.id
    ]

    maintenance_policy {
      auto_upgrade = true
    }
  }

  release_channel         = "STABLE"
  network_policy_provider = "CALICO"

  kms_provider {
    key_id = yandex_kms_symmetric_key.this.id
  }

  depends_on = [
    yandex_resourcemanager_folder_iam_member.k8s_admin,
    yandex_resourcemanager_folder_iam_member.clusters_agent,
    yandex_resourcemanager_folder_iam_member.vpc_public_admin,
    yandex_resourcemanager_folder_iam_member.load_balancer_admin,
    yandex_resourcemanager_folder_iam_member.images_puller
  ]
}

resource "yandex_kubernetes_node_group" "this" {
  cluster_id = yandex_kubernetes_cluster.this.id
  version    = var.k8s_version

  instance_template {
    platform_id = "standard-v3"

    network_interface {
      nat        = true
      subnet_ids = [yandex_vpc_subnet.this.id]

      security_group_ids = [
        yandex_vpc_security_group.main.id,
        yandex_vpc_security_group.nodes_ssh_access.id
      ]
    }

    resources {
      memory = 4
      cores  = 2
    }

    boot_disk {
      type = "network-hdd"
      size = 64
    }

    container_runtime {
      type = "containerd"
    }

    metadata = {
      ssh-keys = "admin:${var.nodes_admin_ssh_public_key} admin"
    }
  }

  scale_policy {
    fixed_scale {
      size = 2
    }
  }

  allocation_policy {
    location {
      zone = "ru-central1-a"
    }
  }

  maintenance_policy {
    auto_upgrade = true
    auto_repair  = true
  }
}
