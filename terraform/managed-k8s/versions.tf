terraform {
  required_version = ">= 1.3.6"

  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
      version = ">= 0.89.0"
    }
  }

  backend "s3" {
    endpoint = "storage.yandexcloud.net"
    region   = "ru-central1"
    key      = "k8s.tfstate"

    skip_region_validation      = true
    skip_credentials_validation = true
  }
}
