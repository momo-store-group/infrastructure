variable "cloud_id" {
  description = "The ID of Cloud to apply any resources to."
  type        = string
  sensitive   = true
}

variable "folder_id" {
  description = "The Folder ID to operate under, if not specified by a given resource."
  type        = string
  sensitive   = true
}

variable "bucket_tfstate" {
  description = "Bucket name for terraform state file. Bucket will be created by terraform."
  type        = string
}

variable "bucket_loki" {
  description = "Bucket name for compressed logs from Loki. Bucket will be created by terraform."
  type        = string
}

variable "bucket_for_images_public" {
  description = "Bucket name for momo-store public images. Bucket will be created by terraform."
  default     = "momo-store-public"
  type        = string
}

variable "zone" {
  description = "The availability zone where the machine will be created."
  default     = "ru-central1-a"
  type        = string

  validation {
    condition     = contains(toset(["ru-central1-a", "ru-central1-b", "ru-central1-c"]), var.zone)
    error_message = "Select availability zone from the list: ru-central1-a, ru-central1-b, ru-central1-c."
  }
}
