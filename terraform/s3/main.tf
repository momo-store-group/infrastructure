# SA

resource "yandex_iam_service_account" "this" {
  name = "s3-editor"
}

resource "yandex_resourcemanager_folder_iam_member" "storage_editor" {
  folder_id = var.folder_id
  role      = "storage.editor"
  member    = "serviceAccount:${yandex_iam_service_account.this.id}"
}

resource "yandex_resourcemanager_folder_iam_member" "kms_encrypter_decrypter" {
  folder_id = var.folder_id
  role      = "kms.keys.encrypterDecrypter"
  member    = "serviceAccount:${yandex_iam_service_account.this.id}"
}

resource "yandex_iam_service_account_static_access_key" "this" {
  service_account_id = yandex_iam_service_account.this.id
  description        = "Static access key for object storage."
}

resource "yandex_kms_symmetric_key" "this" {
  name              = "s3-encryption"
  default_algorithm = "AES_128"
  rotation_period   = "8760h"
}

resource "yandex_storage_bucket" "this" {
  bucket                = var.bucket_tfstate
  access_key            = yandex_iam_service_account_static_access_key.this.access_key
  secret_key            = yandex_iam_service_account_static_access_key.this.secret_key
  acl                   = "private"
  default_storage_class = "STANDARD"
  max_size              = 10485760 # 10 MiB

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = yandex_kms_symmetric_key.this.id
        sse_algorithm     = "aws:kms"
      }
    }
  }

  depends_on = [
    yandex_resourcemanager_folder_iam_member.storage_editor,
    yandex_resourcemanager_folder_iam_member.kms_encrypter_decrypter
  ]
}

resource "yandex_storage_bucket" "loki_storage" {
  bucket                = var.bucket_loki
  access_key            = yandex_iam_service_account_static_access_key.this.access_key
  secret_key            = yandex_iam_service_account_static_access_key.this.secret_key
  acl                   = "private"
  default_storage_class = "STANDARD"
  max_size              = 10485760 # 10 MiB
  depends_on            = [yandex_resourcemanager_folder_iam_member.storage_editor]
}

resource "yandex_storage_bucket" "images_storage" {
  bucket                = var.bucket_for_images_public
  access_key            = yandex_iam_service_account_static_access_key.this.access_key
  secret_key            = yandex_iam_service_account_static_access_key.this.secret_key
  acl                   = "public-read"
  default_storage_class = "STANDARD"
  max_size              = 10485760 # 10 MiB
  depends_on            = [yandex_resourcemanager_folder_iam_member.storage_editor]
}
