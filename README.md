# momo-store infrastructure

Все репозитории, относящиеся к momo-store, объединены в [группе](https://gitlab.com/momo-store-group):

- [Репозиторий с фронтом и бэком](https://gitlab.com/momo-store-group/application)
- [Репозиторий с helm-чартами](https://gitlab.com/momo-store-group/helm-charts)
- [Репозиторий с инфраструктурой](https://gitlab.com/momo-store-group/infrastructure) - текущий

## Install

Вся инфраструктура разворачивается в Yandex Cloud.

<details>
<summary>Почему S3 в отдельном модуле?</summary>

1. Редактирование бакетов не должно влиять на кластер и наоборот
    - Если понадобится свернуть кластер, это не должно удалить бакеты
2. Бакеты могут быть развернуты у другого провайдера
3. State кластера не должен хранится локально => до создания кластера уже должен существовать бакет для бэкенда

</details>

### S3

Создание бакета для tfstate выполняется вручную (единственная манипуляция с инфраструктурой не из пайплайна):

- Для аутентификации в облаке использовать переменную окружения `YC_TOKEN` с токеном, созданным через YC CLI
- Первый init выполняется без бэкенда (бэкенд в `versions.tf` закомментирован)
- Сразу после создания бакета бэкенд в `versions.tf` раскомментируется и init выполняется повторно, загружая state в S3

Данные, которые нужно передать для создания бакетов:

1. ID облака из YC - `var.cloud_id`
2. ID директории для ресурсов из YC - `var.folder_id`
3. Имя бакета, который будет использоваться для хранения state файлов - `var.bucket_tfstate`
4. Имя бакета, который будет использоваться для хранения логов из Loki - `var.bucket_loki`

Опциональные переменные:

1. `var.bucket_for_images_public` - имя публичного бакета, в котором будут лежать картинки для momo-store
    - По-умолчанию `momo-store-public`, это имя используется приложением

```shell
cd terraform/s3

terraform init  # -input=false \
#  -backend-config="bucket=${TFSTATE_BUCKET_NAME}" \
#  -backend-config="access_key=${BUCKET_ACCESS_KEY}" \
#  -backend-config="secret_key=${BUCKET_SECRET_KEY}"

terraform plan -input=false -out="${CI_PROJECT_DIR}/tfplan" \
  -var="cloud_id=${YC_CLOUD_ID}" \
  -var="folder_id=${YC_FOLDER_ID}" \
  -var="bucket_tfstate=tfstate-bucket-name" \
  -var="bucket_loki=loki-bucket-name"

terraform apply -input=false "${CI_PROJECT_DIR}/tfplan"
```

### Managed k8s cluster

Данные, которые нужно передать для развертывания k8s кластера:

1. ID облака из YC - `var.cloud_id`
2. ID директории для ресурсов из YC - `var.folder_id`
3. Список с подсетями, из которых к нодам по SSH будут подключаться админы - `var.nodes_admin_network_cidr_blocks`
4. Публичный SSH ключ для подключения к нодам по SSH `admin@node-nat-ip` - `var.nodes_admin_ssh_public_key`

Опциональные переменные:

1. `var.api_admin_network_cidr_blocks` - список подсетей, из которых к API будут подключаться администраторы
    - По умолчанию `[0.0.0.0/0]` => к API можно подключиться с любого IP

Kubernetes кластер всегда разворачивается с S3 бэкендом => S3 бакет должен быть создан предварительно.

```shell
cd terraform/managed-k8s

terraform init -input=false \
  -backend-config="bucket=${TFSTATE_BUCKET_NAME}" \
  -backend-config="access_key=${BUCKET_ACCESS_KEY}" \
  -backend-config="secret_key=${BUCKET_SECRET_KEY}"

terraform plan -input=false -out="${CI_PROJECT_DIR}/tfplan" \
  -var="cloud_id=${YC_CLOUD_ID}" \
  -var="folder_id=${YC_FOLDER_ID}" \
  -var="nodes_admin_network_cidr_blocks=[\"${YC_NODES_ADMIN_CIDR_BLOCK}\"]" \
  -var="nodes_admin_ssh_public_key=${YC_NODES_ADMIN_SSH_PUBLIC_KEY}"

terraform apply -input=false "${CI_PROJECT_DIR}/tfplan"
```

## Repo

Структура репозитория infrastructure:

```
infrastructure/
├── k8s/ - конфиги, использованные при ручной установке приложений в кластере
│   ├── acme-issuer.yaml
│   ├── gitlab-admin-service-account.yaml
│   ├── grafana.yaml
│   └── trickster.yaml
├── terraform/ - модули terraform
│   ├── managed-k8s/ - модуль для развертывания managed-k8s кластера
│   │   ├── .gitlab-ci.yaml - пайплайн для развертывания и управления managed k8s кластером
│   │   ├── main.tf
│   │   ├── provider.tf
│   │   ├── README.md
│   │   ├── .terraform.lock.hcl
│   │   ├── variables.tf
│   │   └── versions.tf
│   ├── s3/ - модуль для развертывания S3 бакетов
│   │   ├── .gitlab-ci.yaml - пайплайн для управления S3 бакетами 
│   │   ├── main.tf
│   │   ├── provider.tf
│   │   ├── README.md
│   │   ├── .terraform.lock.hcl
│   │   ├── variables.tf
│   │   └── versions.tf
│   └── .gitignore
├── .gitlab-ci.yaml - пайплайн с триггерами
└── README.md
```

## CI

Входные данные - новый коммит в любой ветке.

Результат - у модуля из коммита проверены форматирование и валидность, выведен план внесения изменений.

### Pipeline

1. Проверить форматирование модуля - terraform
2. Проверить валидность модуля - terraform
3. Аутентифицироваться в облаке - `YC_SERVICE_ACCOUNT_KEY_FILE`
4. Вывести план внесения изменений в инфраструктуру - terraform

### Description

Сервисные аккаунты, используемые в GitLab создаются вручную.

Для создания плана используется сервисный аккаунт с viewer правами.

## CD

Входные данные - новый коммит в ветке main.

Результат - план для внесения изменений в инфраструктуру, и задача, применяющая план по кнопке.

**Версия инфраструктуры** - хэш коммита, из которой она развернута

### Pipeline

1. Аутентифицироваться в облаке - `YC_SERVICE_ACCOUNT_KEY_FILE`
2. Создать план для внесения изменений в инфраструктуру (например, развернуть кластер) - terraform
3. Применить изменения по кнопке - terraform

### Description

Переменные с секретами, используемые для развертывания, доступны только из protected веток.

Применение изменений выполняется только по кнопке => continuous delivery.

- Цена ошибки - убитая инфраструктура

Удаление ресурсов выполняется аналогично их созданию (изменить конфиги, MR в main, применить изменения по кнопке).

## Workflow

1. Создать feature-ветку
2. Запушить изменения в feature-ветку
3. В пайплайне проверить измененный модуль с помощью fmt и validate
4. В пайплайне создать план внесения изменений в инфраструктуру
5. Проверить, что планируемые изменения соответствуют изменениям в конфигах
6. Создать MR в main
7. После принятия MR перепроверить план и внести изменения по кнопке

## References

- [Подключиться к Kubernetes API](
  https://cloud.yandex.com/en/docs/managed-kubernetes/operations/connect/#kubectl-connect)
- [Установить в кластере NGINX Ingress контроллер](
  https://cloud.yandex.com/en/docs/managed-kubernetes/tutorials/ingress-cert-manager)
- [Установить в кластере приложения для мониторинга Prometheus и Grafana](
  https://cloud.yandex.com/en/docs/managed-kubernetes/tutorials/prometheus-grafana-monitoring)
- [Установить в кластере сборщик логов Loki](
  https://cloud.yandex.com/en/docs/managed-kubernetes/operations/applications/loki)
- [Установить в кластере ArgoCD](https://cloud.yandex.com/en-ru/docs/managed-kubernetes/operations/applications/argo-cd)
- [Настроить Ingress для ArgoCD](https://github.com/argoproj/argo-cd/blob/master/docs/operator-manual/ingress.md)
- [Создать аккаунт для CI в ArgoCD](https://argo-cd.readthedocs.io/en/stable/operator-manual/user-management/)
- [Сгенерировать токен для аутентификации в ArgoCD из CI](
  https://argo-cd.readthedocs.io/en/latest/user-guide/commands/argocd_account_generate-token/)
- [Ограничить права CI аккаунта из ArgoCD](
  https://argo-cd.readthedocs.io/en/stable/operator-manual/rbac/#rbac-permission-structure)
- [Исправить ошибку в ArgoCD - HPA всегда в OutOfSync](
  https://github.com/argoproj/argo-cd/issues/1079#issuecomment-1375551341)

## TODO

- Уйти от ручной настройки k8s кластера - развернуть ArgoCD и устанавливать другие helm-чарты через него
- Вынести повторяющиеся job в шаблоны
